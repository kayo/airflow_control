#include "fw.h"
#include "upar/params.h"
#include "am2320.h"

//#define SYNC_CONV

#ifdef AM2320_ENABLE
void am2320_i2c_xfer(uint8_t addr, const uint8_t *wptr, size_t wlen, uint8_t *rptr, size_t rlen) {
  hum_i2c_xfer(addr, wptr, wlen, rptr, rlen);
}
#endif /* AM2320_ENABLE */

#ifdef DEBUG_MEM_USAGE
mcu_memory_usage_t mem_usage;
#endif

void ctl_step(void) {
  flow_step();
  cal_step();
  driver_step();

#ifdef DEBUG_MEM_USAGE
  mcu_memory_usage(&mem_usage);
#endif
}

volatile uint8_t events;
uint8_t hum_ev_cnt = 0;

void clk_on_tick(void) {
  iwd_renew();
  
  evt_set(events, sys_ev_rtc);

#ifdef AM2320_ENABLE
  if (++hum_ev_cnt > 10) {
    hum_ev_cnt = 0;
    evt_set(events, hum_ev_get);
  }
#endif /* AM2320_ENABLE */
  
  main_resume();
}

int main(void) {
  irq_init();
  clk_init();
  pio_init();
  adc_init();

  htr_tim_init();
  fan_tim_init();
  hum_i2c_init();
  srt_init();

  upar_init();
  driver_init();

#ifdef SYNC_CONV
  smp_tim_init();
#endif
  
  mcu_modes_on_off(mcu_sleep_on_exit,
                   mcu_event_on_interrupt);
  
  iwd_start();
  iwd_renew();

  for (;;) {
    main_suspend();
    
    uint8_t selected = evt_get(events, any_ev);

    if (evt_has(selected, srt_ev_req)) {
      srt_on_req();
      evt_clr(events, srt_ev_req);
    }

#ifdef AM2320_ENABLE
    if (evt_has(selected, hum_ev_get)) {
      am2320_start();
      clk_wait_us(1500);
      am2320_read(&sens__hum__Hair, &sens__hum__Tair);
      evt_clr(events, hum_ev_get);
    }
#endif /* AM2320_ENABLE */

#ifdef DEBUG_MEM_USAGE
    mcu_memory_usage(&mem_usage);
#endif
  }

  srt_done();
  hum_i2c_done();
  fan_tim_done();
  htr_tim_done();
#ifdef SYNC_CONV
  smp_tim_done();
#endif
  adc_done();
  pio_done();
  clk_done();
  irq_done();

  return 0;
}
