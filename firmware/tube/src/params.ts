import { UParDecl, UParNumCons } from "types";
import { uparServer, uparAttrs } from "param";

interface Attrs {
    language: string,
    version: string,
    hasName: boolean,
    hasInfo: boolean,
    hasUnit: boolean,
    hasItem: boolean,
}

const {
    language,
    version,
    hasName,
    hasInfo,
    hasUnit,
    hasItem,
}: Attrs = uparAttrs({
    language: '_',
    version: '0.0.0',
    hasName: true,
    hasInfo: true,
    hasUnit: true,
    hasItem: true,
});

function l10n(strings: string | { [lang: string]: string }): string {
    if (typeof strings == 'string') {
        return strings;
    } else {
        return strings[language];
    }
}

type option<T> = T | undefined;

function name(str: string): option<string> {
    return hasName ? str : undefined;
}

function info(strings: string | { [lang: string]: string }): option<string> {
    return hasInfo ? l10n(strings) : undefined;
}

function unit(strings: string | { [lang: string]: string }): option<string> {
    return hasUnit ? l10n(strings) : undefined;
}

function item(strings: string | { [lang: string]: string }): option<string> {
    return hasItem ? l10n(strings) : undefined;
}

const byte = {
    bits: 8,
};

const word = {
    bits: 32,
};

const sens = {
    getable: true,
};

const ctrl = {
    ...sens,
    setable: true,
};

const pref = {
    persist: true,
};

const val = {
    type: 'sfix',
    ...word,
    frac: 16,
};

const temp = {
    unit: unit("°C"),
    ...val,
};

const temp_1 = {
    unit: unit("1/°C"),
    ...val,
};

const hum = {
    unit: unit("%"),
    ...val,
};

const volt = {
    unit: unit({
        _: "V",
        ru: "В",
    }),
    ...val,
};

const power = {
    unit: unit({
        _: "W",
        ru: "Вт",
    }),
    min: 0,
    ...val,
};

const sec = {
    unit: unit({
        _: "S",
        ru: "с",
    }),
    ...val,
};

const part = {
    min: 0.0, max: 1.0,
    ...val,
};

const flow = {
    ...part,
};

const temp_sec = {
    unit: unit({
        _: "°C/S",
        ru: "°C/с",
    }),
    ...val,
};

const mode = {
    type: 'uint',
    ...byte,
};

const logic = {
    opt: [
        {
            name: name('no'),
            val: 'false',
            info: info({
                _: "No",
                ru: "Нет",
            }),
        },
        {
            name: name('yes'),
            val: 'true',
            info: info({
                _: "Yes",
                ru: "Да",
            }),
        },
    ],
    ...mode
};

const virt = {
    bind: true,
};

interface PIDParamsConfig {
    Kp?: UParNumCons;
    Ti?: UParNumCons;
    Td?: UParNumCons;
    Ei_lim?: UParNumCons;
}

function pid_params(pfx: string, cfg: PIDParamsConfig = {}): UParDecl {
    return {
        name: name('pid'),
        info: info({
            _: "PID tuning",
            ru: "Настройка PID",
        }),
        params: [
            {
                name: name('Kp'),
                info: info({
                    _: "Proportional factor",
                    ru: "Пропорциональный коэффициент",
                }),
                ...temp_1, ...ctrl, ...pref, ...(cfg.Kp || {}),
                postSet: `pid_set_Kp(VAL_T, &${pfx}_pid_param, <var>);`,
            },
            {
                name: name('Ti'),
                info: info({
                    _: "Integral factor",
                    ru: "Интегральный коэффициент",
                }),
                ...sec, ...ctrl, ...pref, ...(cfg.Ti || {}),
                min: 0.0, def: 90.0,
                postSet: `pid_set_Ti(VAL_T, &${pfx}_pid_param, <var>, CTL_PERIOD_VAL);`,
            },
            {
                name: name('Td'),
                info: info({
                    _: "Derivative factor",
                    ru: "Дифференциальный коэффициент",
                }),
                ...sec, ...ctrl, ...pref, ...(cfg.Td || {}),
                min: 0.0, def: 10.0,
                postSet: `pid_set_Td(VAL_T, &${pfx}_pid_param, <var>, CTL_PERIOD_VAL);`,
            },
            {
                name: name('Ei_lim'),
                info: info({
                    _: "Integral error limit",
                    ru: "Ограничение интегральной ошибки",
                }),
                ...temp, ...ctrl, ...pref, ...(cfg.Ei_lim || {}),
                min: 0.0, def: 5.0,
                postSet: `pid_set_Ei_limit(VAL_T, &${pfx}_pid_param, <var>);`,
            },
        ],
    };
}

interface LookupPointConfig {
    len: number;
    arg?: UParDecl;
    res?: UParDecl;
}

function lookup_points(cfg: LookupPointConfig): UParDecl[] {
    const arg = cfg.arg || {};
    const res = cfg.res || {};
    const params: UParDecl[] = [
        {
            name: name('from'),
            ...arg,
        },
        {
            name: name('step'),
            ...arg,
        },
    ];
    for (let i = 0; i < cfg.len; i++) {
        params.push({
            name: name(`${i}`),
            ...res,
            ...(res.bind ? { bind: `${res.bind}[${i}]` } : {})
        });
    }
    return params;
}

uparServer({
    includes: [
        "fw.h",
    ],

    storage: {
        backend: 'mcuhw',
        uidType: 'djb2',
        section: 'par',
    },

    params: [
        {
            name: name('sens'),
            info: info({
                _: "Environment state",
                ru: "Состояние среды",
            }),
            item: item({
                _: "Environ",
                ru: "Среда",
            }),
            params: [
                {
                    name: name('Flow'),
                    info: info({
                        _: "Air flow",
                        ru: "Воздушный поток",
                    }),
                    params: [
                        {
                            name: name('value'),
                            info: info({
                                _: "Value",
                                ru: "Значение",
                            }),
                            ...flow, ...sens,
                        },
                        {
                            name: name('raw'),
                            info: info({
                                _: "Raw value",
                                ru: "Сырое значение",
                            }),
                            ...flow, ...sens,
                        },
                    ]
                },
                {
                    name: name('Tair'),
                    info: info({
                        _: "Air temperature",
                        ru: "Температура воздуха",
                    }),
                    params: [
                        {
                            name: name('ref'),
                            info: info({
                                _: "Reference",
                                ru: "Опорная",
                            }),
                            ...temp, ...sens,
                        },
                        {
                            name: name('front'),
                            info: info({
                                _: "Front",
                                ru: "Передняя",
                            }),
                            ...temp, ...sens,
                        },
                        {
                            name: name('rear'),
                            info: info({
                                _: "Rear",
                                ru: "Тыловая",
                            }),
                            ...temp, ...sens,
                        },
                        {
                            name: name('stable'),
                            info: info({
                                _: "Stability",
                                ru: "Стабильность",
                            }),
                            ...logic, ...sens,
                        },
                    ],
                },
                {
                    name: name('Tair_dif'),
                    info: info({
                        _: "Air temperature vibration",
                        ru: "Колебания температуры воздуха",
                    }),
                    params: [
                        {
                            name: name('front'),
                            info: info({
                                _: "Front",
                                ru: "Передняя",
                            }),
                            ...temp_sec, ...sens,
                        },
                        {
                            name: name('rear'),
                            info: info({
                                _: "Rear",
                                ru: "Тыловая",
                            }),
                            ...temp_sec, ...sens,
                        }
                    ],
                },
                {
                    name: name('hum'),
                    info: info({
                        _: "Humidity sensor",
                        ru: "Датчик влажности",
                    }),
                    params: [
                        {
                            name: name('Tair'),
                            info: info({
                                _: "Air temperature",
                                ru: "Температура воздуха",
                            }),
                            ...temp, ...sens,
                        },
                        {
                            name: name('Hair'),
                            info: info({
                                _: "Air humidity",
                                ru: "Влажность воздуха",
                            }),
                            ...hum, ...sens,
                        }
                    ],
                },
            ],
        },
        {
            name: name('ctrl'),
            info: info({
                _: "Control state",
                ru: "Состояние управления",
            }),
            item: item({
                _: "State",
                ru: "Состояние",
            }),
            params: [
                {
                    name: name('op'),
                    info: info({
                        _: "Current operation",
                        ru: "Текущая операция",
                    }),
                    ...mode, ...sens, ...ctrl,
                    //postSet: "dif_iter = 0;",
                    def: 'find_ref_op',
                    //def: 'reg_flow_op',
                    opt: [
                        {
                            name: name('reg_flow'),
                            val: 'reg_flow_op',
                            info: info({
                                _: "Air flow regulation",
                                ru: "Регулировка воздушного потока",
                            }),
                        },
                        {
                            name: name('find_ref'),
                            val: 'find_ref_op',
                            info: info({
                                _: "Reference temperature searching",
                                ru: "Поиск опорной температуры",
                            }),
                        },
                        {
                            name: name('cal_flow'),
                            val: 'cal_flow_op',
                            info: info({
                                _: "Flow sensor calibration",
                                ru: "Калибровка датчика потока",
                            }),
                        },
                    ],
                },
                {
                    name: name('Phtr'),
                    info: info({
                        _: "Sensor heater power",
                        ru: "Мощность нагревателя датчика",
                    }),
                    ...power, ...sens,
                    min: 0.0, max: 'HTR_PWR',
                },
                {
                    name: name('Dfan'),
                    info: info({
                        _: "Fan duty cycle",
                        ru: "Рабочий цикл вентилятора",
                    }),
                    ...part, ...sens,
                    min: 0.0, max: 1.0,
                },
            ]
        },
        {
            name: name('conf'),
            info: info({
                _: "Main controls",
                ru: "Основные настройки",
            }),
            params: [
                {
                    name: name('state'),
                    info: info({
                        _: "Control state",
                        ru: "Состояние управления",
                    }),
                    ...mode, ...ctrl, ...pref,
                    def: 'enabled_state',
                    opt: [
                        {
                            name: name('disabled'),
                            val: 'disabled_state',
                            info: info({
                                _: "Disabled",
                                ru: "Выключено",
                            }),
                        },
                        {
                            name: name('enabled'),
                            val: 'enabled_state',
                            info: info({
                                _: "Enabled",
                                ru: "Включено",
                            }),
                        },
                    ],
                },
                {
                    name: name('mode'),
                    info: info({
                        _: "Control mode",
                        ru: "Режим управления",
                    }),
                    ...mode, ...ctrl, ...pref,
                    def: 'auto_mode',
                    opt: [
                        {
                            name: name('auto'),
                            val: 'auto_mode',
                            info: info({
                                _: "Automatic",
                                ru: "Автоматический",
                            }),
                        },
                        {
                            name: name('manual'),
                            val: 'manual_mode',
                            info: info({
                                _: "Manual",
                                ru: "Ручной",
                            }),
                        },
                    ],
                },
                {
                    name: name('Flow'),
                    info: info({
                        _: "Air flow",
                        ru: "Воздушный поток",
                    }),
                    params: [
                        {
                            name: name('normal'),
                            info: info({
                                _: "Normal",
                                ru: "Обычный",
                            }),
                            def: 0.1, min: 0.0, max: 1.0,
                            ...flow, ...ctrl, ...pref,
                        },
                        {
                            name: name('forced'),
                            info: info({
                                _: "Forced",
                                ru: "Усиленный",
                            }),
                            def: 0.5, min: 0.0, max: 1.0,
                            ...flow, ...ctrl, ...pref,
                        },
                    ],
                },
                {
                    name: name('Dfan'),
                    info: info({
                        _: "Fan duty cycle",
                        ru: "Рабочий цикл вентилятора",
                    }),
                    ...part, ...ctrl,
                    min: 0.0, max: 1.0, def: 0.8,
                },
                {
                    name: name('Phtr'),
                    info: info({
                        _: "Heater power",
                        ru: "Мощность нагревателя",
                    }),
                    ...power, ...ctrl, ...pref,
                    min: 0.0, max: 'HTR_PWR', def: 'HTR_PWR',
                },
            ],
        },

        {
            name: name('int'),
            info: info({
                _: "Internal state",
                ru: "Внутреннее состояние",
            }),
            params: [
                {
                    name: name('Uref'),
                    info: info({
                        _: "Reference voltage",
                        ru: "Опорное напряжение",
                    }),
                    ...volt, ...sens,
                },
                {
                    name: name('Tmcu'),
                    info: info({
                        _: "Microcontroller temperature",
                        ru: "Температура микроконтроллера",
                    }),
                    ...temp, ...sens,
                },
            ],
        },

        {
            name: name('tun'),
            info: info({
                _: "Fine tuning",
                ru: "Тонкая настройка",
            }),
            params: [
                {
                    name: name('sens'),
                    info: info({
                        _: "Sensors",
                        ru: "Датчики",
                    }),
                    params: [
                        {
                            name: name('Tair_int'),
                            info: info({
                                _: "Filtering interval for environment temperature",
                                ru: "Интервал фильтрации для температуры среды",
                            }),
                            ...sec, ...ctrl, ...pref,
                            def: 30, min: 0, max: 1000,
                            postSet: "t_flt_set_T(VAL_T, &Tair_flt_param, <var>, CTL_PERIOD_VAL);",
                        },
                        {
                            name: name('Tair_dif_int'),
                            info: info({
                                _: "Filtering interval for temperature vibration",
                                ru: "Интервал фильтрации для колебаний температуры",
                            }),
                            ...sec, ...ctrl, ...pref,
                            def: 10, min: 0, max: 100,
                            postSet: "t_flt_set_T(VAL_T, &Tair_dif_flt_param, <var>, CTL_PERIOD_VAL);",
                        },
                        {
                            name: name('Tair_stab_thres'),
                            info: info({
                                _: "Temperature stability threshold",
                                ru: "Порог стабильности температуры",
                            }),
                            ...temp_sec, ...ctrl, ...pref,
                            def: 0.03, min: 0.0, max: 1.0,
                            postSet: "calc_Tair_stab_range();",
                        },
                        {
                            name: name('Tair_stab_hist'),
                            info: info({
                                _: "Temperature stability gisteresis",
                                ru: "Гистерезис стабильности температуры",
                            }),
                            ...temp_sec, ...ctrl, ...pref,
                            def: 0.01, min: 0.0, max: 1.0,
                            postSet: "calc_Tair_stab_range();",
                        },
                        {
                            name: name('Flow_lookup'),
                            info: info({
                                _: "Flow calibration table",
                                ru: "Таблица калибровки потока",
                            }),
                            params: lookup_points({
                                len: 16,
                                arg: {
                                    ...part, ...ctrl, ...pref,
                                    min: 0.0, max: 1.0,
                                },
                                res: {
                                    ...part, ...ctrl, ...pref,
                                    min: 0.0, max: 1.0,
                                    bind: 'tun__sens__Flow_lookup__data',
                                }
                            }),
                        },
                    ],
                },
                {
                    name: name('fan'),
                    info: info({
                        _: "Fan",
                        ru: "Вентилятор",
                    }),
                    params: [
                        pid_params('fan', {
                            Kp: { def: 0.2, min: 0, max: 1 },
                            Ti: { def: 90, min: 0 },
                            Td: { def: 10, min: 0 },
                            Ei_lim: { def: 5, min: 0 }
                        }),
                    ],
                },
            ],
        },

        {
            name: name('info'),
            info: info({
                _: "Device info",
                ru: "Информация об устройстве",
            }),
            params: [
                {
                    name: name('device'),
                    info: info({
                        _: "Device name",
                        ru: "Название устройства",
                    }),
                    type: 'cstr',
                    def: l10n({
                        _: "The climate control unit",
                        ru: "Устройство управления климатом",
                    }),
                    ...virt,
                },
                {
                    name: name('version'),
                    info: info({
                        _: "Version number",
                        ru: "Номер версии",
                    }),
                    type: 'cstr',
                    def: version,
                    ...virt,
                },
            ],
        },
    ],
});
