#include "fw.h"
#include "hw.h"
#include "am2320_def.h"
#include "upar/params.h"

void am2320_i2c_xfer(uint8_t addr, const uint8_t *wptr, size_t wlen, uint8_t *rptr, size_t rlen) {
  hum_i2c_xfer(addr, wptr, wlen, rptr, rlen);
}

void hum_read(void) {
  am2320_read(&sens__hum__Hair, &sens__hum__Tair);
}
