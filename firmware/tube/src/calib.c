#include <ctl/type.h>
#include <ctl/util.h>

#include "fw.h"
#include "upar/params.h"

uint8_t cal_iter;

static uint8_t _ctrl__op = find_ref_op;
static enum {
  cal_seek,
  cal_stop,
} cal_stage;

void cal_step(void) {
  bool op_changed = false;
  
  if (_ctrl__op != ctrl__op) {
    _ctrl__op = ctrl__op;
    op_changed = true;
  }
  
  switch (ctrl__op) {
  case find_ref_op:
    if (sens__Tair__stable) {
      sens__Tair__ref = min_val(VAL_T, sens__Tair__front, sens__Tair__rear);
      ctrl__op = reg_flow_op;
    }
    break;
  case cal_flow_op:
    if (op_changed) {
      cal_stage = cal_seek;
      cal_iter = 0;
    }
    switch (cal_stage) {
    case cal_seek:
      if (!sens__Tair__stable) {
        cal_stage = cal_stop;
      }
      break;
    case cal_stop:
      if (sens__Tair__stable) {
        tun__sens__Flow_lookup__data[cal_iter] = sens__Flow__raw;
        cal_stage = cal_seek;
        if (cal_iter < 16 - 1) {
          cal_iter ++;
        } else {
          ctrl__op = find_ref_op;
        }
      }
      break;
    }
    break;
  }
}
