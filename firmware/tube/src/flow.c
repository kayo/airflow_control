#include <ctl/type.h>
#include <ctl/util.h>

#include <ctl/dif.h>

#include "fw.h"
#include "upar/params.h"

#ifdef FLOW_LOOKUP
#include "flow-lookup.h"
#endif

#define toH(type, val)                                   \
  gen_int(type,                                          \
          scale_val_const(type,                          \
                          clamp_val_const(type,          \
                                          val,           \
                                          0.0, HTR_PWR), \
                          0.0, HTR_PWR,                  \
                          0, HTR_TIM_TOP - 1))

void flow_init(void) {
  //htr_tim_SENS_HT_put(toH(VAL_T, gen_1(VAL_T)));
}

VAL_T tun__sens__Flow_lookup__data[16];

//static VAL_T dif__Tair__front, dif__Tair__rear;
#define dif__Tair__front sens__Tair_dif__front
#define dif__Tair__rear sens__Tair_dif__rear
static bool dif_temp(void);

VAL_T tun__sens__Tair_stab__range[2];

void calc_Tair_stab_range(void) {
  tun__sens__Tair_stab__range[0] =
    gen_sub(VAL_T,
            tun__sens__Tair_stab_thres,
            tun__sens__Tair_stab_hist);
  tun__sens__Tair_stab__range[1] =
    gen_add(VAL_T,
            tun__sens__Tair_stab_thres,
            tun__sens__Tair_stab_hist);
}

void flow_step(void) {
  if (ctrl__op == find_ref_op) {
    ctrl__Phtr = gen_0(VAL_T);
  } else {
    ctrl__Phtr = conf__Phtr;
    
    VAL_T Tmin = min_val(VAL_T, sens__Tair__front, sens__Tair__rear);
    VAL_T Flow = gen_div(VAL_T, 
                         gen_sub(VAL_T, Tmin, sens__Tair__ref),
                         ctrl__Phtr);

    sens__Flow__raw = gen_lt(VAL_T, sens__Tair__front, sens__Tair__rear) ?
      Flow : gen_neg(VAL_T, Flow);

    Flow = lookup_arg(VAL_T, tun__sens__Flow_lookup_, Flow);
    
    sens__Flow__value = gen_lt(VAL_T, sens__Tair__front, sens__Tair__rear) ?
      Flow : gen_neg(VAL_T, Flow);
  }

  htr_tim_SENS_HT_put(toH(VAL_T, ctrl__Phtr));

  if (/*ctrl__op != reg_flow_op && */dif_temp()) {
    VAL_T abs_dif__Tair__front = abs_val(VAL_T, dif__Tair__front);
    VAL_T abs_dif__Tair__rear = abs_val(VAL_T, dif__Tair__rear);
    
    if (sens__Tair__stable) {
      if (gen_gt(VAL_T,
                 abs_dif__Tair__front,
                 tun__sens__Tair_stab__range[1]) &&
          gen_gt(VAL_T,
                 abs_dif__Tair__rear,
                 tun__sens__Tair_stab__range[1])) {
        sens__Tair__stable = false;
      }
    } else {
      if (gen_lt(VAL_T,
                 abs_dif__Tair__front,
                 tun__sens__Tair_stab__range[0]) &&
          gen_lt(VAL_T,
                 abs_dif__Tair__rear,
                 tun__sens__Tair_stab__range[0])) {
        sens__Tair__stable = true;
      }
    }
  }
}

typedef dif_param_t(VAL_T) dif_param_t;
typedef dif_state_t(VAL_T) dif_state_t;

static dif_param_t Tair_dif_param = dif_param(VAL_T, CTL_PERIOD_VAL);

static dif_state_t dif__Tair__front_state, dif__Tair__rear_state;

Tflt_param_t Tair_dif_flt_param;
static Tflt_state_t dif__Tair__front__flt_state, dif__Tair__rear__flt_state;

uint8_t dif_iter;

static bool dif_temp(void) {
  if (dif_iter > 0) {
    VAL_T raw_dif__Tair__front = dif_step(VAL_T, &Tair_dif_param, &dif__Tair__front_state, sens__Tair__front);
    VAL_T raw_dif__Tair__rear = dif_step(VAL_T, &Tair_dif_param, &dif__Tair__rear_state, sens__Tair__rear);
    if (dif_iter > 1) {
      dif__Tair__front = t_flt_step(VAL_T, &Tair_dif_flt_param, &dif__Tair__front__flt_state, raw_dif__Tair__front);
      dif__Tair__rear = t_flt_step(VAL_T, &Tair_dif_flt_param, &dif__Tair__rear__flt_state, raw_dif__Tair__rear);
      return true;
    } else {
      t_flt_init(VAL_T, &dif__Tair__front__flt_state, raw_dif__Tair__front);
      t_flt_init(VAL_T, &dif__Tair__rear__flt_state, raw_dif__Tair__rear);
      dif_iter ++;
    }
  } else {
    dif_init(VAL_T, &dif__Tair__front_state, sens__Tair__front);
    dif_init(VAL_T, &dif__Tair__rear_state, sens__Tair__rear);
    dif_iter ++;
  }
  return false;
}
