#include <ctl/type.h>
#include <ctl/util.h>
#include <ctl/med.h>

#include "hw.h"
#include "fw.h"
#include "upar/params.h"

#define toU(type, a)                     \
  scale_val_const(type, a,               \
                  ADC_MIN, ADC_MAX,      \
                  ADC_U_MIN, ADC_U_MAX)

#define toA(type, v)                     \
  scale_val_const(type, v,               \
                  ADC_U_MIN, ADC_U_MAX,  \
                  ADC_MIN, ADC_MAX)

#if 1
#define Avg_Slope 4.3
#define V30 1.43

/* T = (30 + V30 / Avg_Slope) - adc * (ADC_U_MAX / (ADC_MAX * Avg_Slope)) */
#define toTmcu(type, adc)                           \
  gen_sub(type,                                     \
          gen_make(type, V30 / Avg_Slope + 30),     \
          gen_mul(type, adc,                        \
                  gen_make(type, ADC_U_MAX /        \
                           (ADC_MAX * Avg_Slope))))
#else
#define toTmcu(type, adc)                               \
  scale_val(type, adc,                                  \
            gen_make(type, adc_cal_temp_30C_at_3V3()),  \
            gen_make(type, adc_cal_temp_110C_at_3V3()), \
            gen_make(type, 30), gen_make(type, 110))
#endif

#define A2D(A) (((double)(A) - (double)(ADC_MIN)) * (1.0 / ((double)(ADC_MAX) - (double)(ADC_MIN))))
#define D2A(D) ((D) * ((double)(ADC_MAX) - (double)(ADC_MIN)) + (double)(ADC_MIN))

#include "thermistor.h"

#define T_A2R(A) A2R_HI(A, T_RS, T_RP)
#define T_R2A(R) R2A_HI(R, T_RS, T_RP)

#define T_T2A(C) T_R2A(BETA_K2R(C2K(C), T_BETA, T_RN, C2K(T_NOM)))
#define T_A2T(A) K2C(BETA_R2K(T_A2R(A), T_BETA, T_RN, C2K(T_NOM)))

#include "thermistor-lookup.h"

#define toT(type, A) lookup_val(type, T, A)

Tflt_param_t Tair_flt_param;
#define Tflt_state(TX) TX##_flt_state
static Tflt_state_t Tflt_state(sens__Tair__front), Tflt_state(sens__Tair__rear);
#ifdef DEDICATED_REFERENCE
static Tflt_state_t Tflt_state(sens__Tair__ref);
#endif

#define Tflt_init(TX) t_flt_init(VAL_T, &TX##_flt_state, TX = raw_##TX)
#define Tflt_step(TX, PX) TX = t_flt_step(VAL_T, &PX##_flt_param, &TX##_flt_state, raw_##TX)

bool inited = false;

static void adc_conv(VAL_T data[]) {
  int__Uref = toU(VAL_T, data[ADC_INDEX_U_REF]);
  int__Tmcu = toTmcu(VAL_T, data[ADC_INDEX_T_MCU]);

  VAL_T raw_sens__Tair__front = toT(VAL_T, data[ADC_INDEX_T_0]);
  VAL_T raw_sens__Tair__rear = toT(VAL_T, data[ADC_INDEX_T_1]);
#ifdef DEDICATED_REFERENCE
  VAL_T raw_sens__Tair__ref = toT(VAL_T, data[ADC_INDEX_T_E]);
#endif

  if (inited) {
    Tflt_step(sens__Tair__front, Tair);
    Tflt_step(sens__Tair__rear, Tair);
#ifdef DEDICATED_REFERENCE
    Tflt_step(sens__Tair__ref, Tair);
#endif
  } else {
    Tflt_init(sens__Tair__front);
    Tflt_init(sens__Tair__rear);
#ifdef DEDICATED_REFERENCE
    Tflt_init(sens__Tair__ref);
#endif
  }

  /* control step */
  ctl_step();
}

void adc_dma_on_full(void) {
  adc_stop();

#ifdef DEBUG_SENSOR
  pio_PWR_GATE_clr();
#endif
  
  VAL_T data[ADC_CHANNELS];
  uint8_t sample;
  uint8_t channel;
  
  for (channel = 0; channel < ADC_CHANNELS; channel ++) {
    VAL_T buf[ADC_SAMPLES];
    
    for (sample = 0; sample < ADC_SAMPLES; sample++) {
      buf[sample] = gen_make(VAL_T, adc_dma_data[sample]._[channel]);
    }

    data[channel] = scale_val_const(VAL_T,
                                    med_step(VAL_T, buf, ADC_SAMPLES, ADC_SAMPLES / 3),
                                    ADC_MIN, ADC_MAX,
                                    0, (1<<12)-1);
  }

  adc_conv(data);
}
