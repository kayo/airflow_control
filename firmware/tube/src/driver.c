#include <ctl/type.h>
#include <ctl/util.h>

#include "hw.h"
#include "fw.h"
#include "upar/params.h"

void driver_init(void) {
  //fan_tim_ADC_TRIG_put(FAN_TIM_TOP - 1);
  fan_tim_ADC_TRIG_put(5);

#ifdef DEBUG_DRIVER
  fan_tim_PWR_GATE_put(256);
#endif
}

static uint8_t periods = 0;

void pio_ZERO_DET_on_edge(void) {
#ifdef DEBUG_SENSOR
  pio_PWR_GATE_set();
#endif
  periods = 0;
}

void fan_tim_on_update(void) {
  if (++periods > 10) {
    fan_tim_stop();
  }
}

#include "dimmer.h"

#define D_D2C(D) (DIM_LIN2SIN(D) * FAN_TIM_TOP)

#include "dimmer-lookup.h"

#define toC(type, d)                            \
  gen_int(type,                                 \
          lookup_val(type, D, d))

fan_pid_param_t fan_pid_param;
pid_state_t(VAL_T) fan_pid_state;

static VAL_T fan_auto(void) {
  VAL_T Flow_error = error_val(VAL_T, conf__Flow__normal, sens__Flow__value);
  VAL_T Dfan_raw = pid_step(VAL_T, &fan_pid_param, &fan_pid_state, Flow_error);

  return gen_gt(VAL_T, Dfan_raw, gen_0(VAL_T)) ?
    clamp_val_const(VAL_T, Dfan_raw, FAN_MIN, FAN_MAX) :
    gen_0(VAL_T);
}

void driver_step(void) {
  if (ctrl__op == reg_flow_op) {
    ctrl__Dfan =
      conf__state == disabled_state ? gen_0(VAL_T) :
      conf__mode == manual_mode ? conf__Dfan :
      fan_auto();
  } else if (ctrl__op == cal_flow_op) {
    ctrl__Dfan = gen_add(VAL_T,
                         gen_mul(VAL_T, tun__sens__Flow_lookup__step,
                                 gen_make(VAL_T, cal_iter)),
                         tun__sens__Flow_lookup__from);
  }

  fan_tim_PWR_GATE_put(toC(VAL_T, ctrl__Dfan));
}
