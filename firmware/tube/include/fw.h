#ifndef FW_H
#define FW_H

#define T_FLT_EWMA 1
#define T_FLT_PT1  2

#include <handler/evt.h>
#if T_FLT == T_FLT_PT1
#include <ctl/pt1.h>
#endif
#if T_FLT == T_FLT_EWMA
#include <ctl/ewma.h>
#endif
#include <ctl/pid.h>
#include <hw.h>

#define CTL_PERIOD_VAL gen_make(VAL_T, CTL_PERIOD)

typedef enum control_state {
  disabled_state,
  enabled_state,
} control_state_t;

typedef enum control_mode {
  manual_mode,
  auto_mode,
} control_mode_t;

typedef enum control_op {
  reg_flow_op, /* automatic flow regulation */
  cal_flow_op, /* calibrate flow vs fan power */
  find_ref_op, /* find reference temperature */
} control_op_t;

#if T_FLT == T_FLT_PT1
typedef pt1_param_t(VAL_T) Tflt_param_t;
typedef pt1_state_t(VAL_T) Tflt_state_t;
#define t_flt_set_T pt1_set_T
#define t_flt_init pt1_init
#define t_flt_step pt1_step
#endif

#if T_FLT == T_FLT_EWMA
typedef ewma_param_t(VAL_T) Tflt_param_t;
typedef ewma_state_t(VAL_T) Tflt_state_t;
#define t_flt_set_T ewma_set_T
#define t_flt_init ewma_init
#define t_flt_step ewma_step
#endif

extern Tflt_param_t Tair_flt_param;
extern Tflt_param_t Tair_dif_flt_param;

extern uint8_t cal_iter; /* calibration iteration */
extern uint8_t dif_iter; /* derivative iteration */

void cal_step(void);
void ctl_step(void);

#define temp_valid(T) gen_gt(VAL_T, T, gen_make(VAL_T, T_INVALID))

void hum_read(void);

extern VAL_T tun__sens__Tair_stab__range[2];
void calc_Tair_stab_range(void);

extern VAL_T tun__sens__Flow_lookup__data[16];
void flow_init(void);
void flow_step(void);

typedef pid_param_t(VAL_T) fan_pid_param_t;
extern fan_pid_param_t fan_pid_param;

void driver_init(void);
void driver_step(void);

enum {
  any_ev = 0xff,
  sys_ev_rtc = 1 << 0,
  srt_ev_req = 1 << 1,
  hum_ev_get = 1 << 2,
};

#define main_resume()                    \
  mcu_modes_on_off(mcu_modes_notouch,    \
                   mcu_sleep_on_exit)

#define main_suspend()                 \
  mcu_modes_on_off(mcu_sleep_on_exit,  \
                   mcu_modes_notouch); \
  mcu_wait_for_interrupt()

extern volatile uint8_t events;

void srt_on_req(void);

#endif /* FW_H */
