#ifndef DIMMER_H
#define DIMMER_H

/* linear power control */
/*
  Q[U]: sin(%pi * t);
  Q[P]: (1 - cos(%pi * t)) / 2;
  wxplot2d([Q[U], Q[P]], [t, 0, 1]);
  Q[C]: rhs(solve(x = Q[P], t)[1]);
  wxplot2d([Q[C]], [x, 0, 1]);
*/
/*
  EQ[p]: p=sin(%pi*t)^2;
  wxplot2d([rhs(EQ[p])], [t, 0, 1]);
  EQ[t]: solve(EQ[p], t)[2];
  wxplot2d([2*rhs(EQ[t])], [p, 0, 1]);
*/

#define DIM_LIN2SIN(D) (1.0 - acos(2.0 * (D) - 1.0) / M_PI)

#define DIM_SQR2SIN(D) (2.0 * asin(sqrt(D)) / M_PI)

#endif /* DIMMER_H */
