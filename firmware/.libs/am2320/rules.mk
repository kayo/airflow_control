libam2320.BASEPATH := $(dir $(lastword $(MAKEFILE_LIST)))

TARGET.LIBS += libam2320
libam2320.INHERIT := firmware libcontrol
libam2320.CDIRS := $(libam2320.BASEPATH)include
libam2320.SRCS := $(addprefix $(libam2320.BASEPATH)src/,\
  am2320.c)
