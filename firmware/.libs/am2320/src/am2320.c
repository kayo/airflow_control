#include <am2320.h>
#include "private/am2320_def.h"

static uint16_t am2320_crc16(const uint8_t *ptr, uint8_t len) {
  uint16_t crc =0xFFFF;
  uint8_t i;
  for (; len--; ) {
    crc ^=*ptr++;
    for (i=0; i<8; i++) {
      if (crc & 0x01) {
        crc>>=1;
        crc^=0xA001;
      } else {
        crc>>=1;
      }
    }
  }
  return crc;
}

typedef struct {
  uint8_t high;
  uint8_t low;
} be16_t;

typedef struct {
  uint8_t low;
  uint8_t high;
} le16_t;

#define to16(val) (val.low | (val.high << 8))

void am2320_start(void) {
  /* wakeup */
  am2320_i2c_xfer(AM2320_I2C_ADDR, (const uint8_t*)1, 0, NULL, 0);

  /* data exchange */
  
  const struct {
    uint8_t func;
    uint8_t addr;
    uint8_t size;
  } send_data = {
    AM2320_FUNC_READ,
    AM2320_ADDR_HUMIDITY,
    AM2320_SIZE_HUMIDITY + AM2320_SIZE_TEMPERATURE,
  };
  
  am2320_i2c_xfer(AM2320_I2C_ADDR, (const uint8_t *)&send_data, sizeof(send_data), NULL, 0);
}

int am2320_read(fix16 *humidity, fix16 *temperature) {
  struct {
    uint8_t func;
    uint8_t size;
    be16_t humidity;
    be16_t temperature;
    le16_t crc;
  } recv_data;
  
  am2320_i2c_xfer(AM2320_I2C_ADDR, NULL, 0, (uint8_t *)&recv_data, sizeof(recv_data));

  if (to16(recv_data.crc) !=
    am2320_crc16((const uint8_t *)&recv_data,
    sizeof(recv_data) - sizeof(recv_data.crc))) {
    return -1;
  }

#define conv(val, msk) gen_mul(fix16, gen_make(fix16, 0.1), gen_make(fix16, to16(val) & msk))

  if (humidity) {
    *humidity = conv(recv_data.humidity, ~0);
  }

  if (temperature) {
    *temperature = conv(recv_data.temperature, ~AM2320_SIGN_TEMPERATURE);
    if (recv_data.temperature.high & (AM2320_SIGN_TEMPERATURE >> 8)) {
      *temperature = gen_neg(fix16, *temperature);
    }
  }
  
  return 0;
}
