#ifndef __AM2320_H__
#define __AM2320_H__

#include <ctl/type.h>

void am2320_i2c_xfer(uint8_t addr, const uint8_t *wptr, size_t wlen, uint8_t *rptr, size_t rlen);
void am2320_start(void);
int am2320_read(fix16 *humidity, fix16 *temperature);

#endif /* __AM2320_H__ */
