#ifndef __HANDLER__BTN_H__
#define __HANDLER__BTN_H__

#include <handler/pio.h>

#define _btn_vr(btn, var) _btn_##btn##_##var
#define btn_vr(btn, var) _btn_vr(btn, var)

#define _btn_fn(btn, op, ...) btn_##btn##_##op(__VA_ARGS__)
#define btn_fn(btn, op, ...) _btn_fn(btn, op, ##__VA_ARGS__)

#define btn_def(btn)                    \
  static int16_t btn_vr(btn, pushed);   \
  static int16_t btn_vr(btn, released); \
  static void                           \
  btn_fn(btn, init, void) {             \
    btn_vr(btn, pushed) = -1;           \
    btn_vr(btn, released) = -1;         \
  }                                     \
  void pio_fn(btn, on_edge, void) {     \
    if (pio_fn(btn, get)) {             \
      /* button pushed */               \
      btn_vr(btn, pushed) = 0;          \
      btn_vr(btn, released) = -1;       \
    } else {                            \
      /* button released */             \
      btn_vr(btn, released) =           \
        btn_vr(btn, pushed);            \
      btn_vr(btn, pushed) = -1;         \
    }                                   \
  }

#define btn_on(btn, delta_)           \
  for (int16_t btn_vr(btn, delta) =   \
         (delta_);                    \
       (btn_vr(btn, pushed) != -1 ||  \
        btn_vr(btn, released) != -1)  \
         && btn_vr(btn, delta) > 0;   \
       btn_vr(btn, pushed) =          \
         btn_vr(btn, pushed) != -1 ?  \
         btn_vr(btn, pushed) +        \
         (delta_) : -1,               \
         btn_vr(btn, released) = -1,  \
         btn_vr(btn, delta) = 0)

#define btn_pushed(btn, delay)          \
  if (btn_vr(btn, pushed) != -1 &&      \
      btn_vr(btn, pushed) >= (delay) && \
      btn_vr(btn, pushed) < (delay) +   \
      (btn_vr(btn, delta)))

#define btn_released(btn, from, to)     \
  if (btn_vr(btn, released) != -1 &&    \
      btn_vr(btn, released) >= (from)   \
      && btn_vr(btn, released) < (to))

#endif /* __HANDLER__BTN_H__ */
