#ifndef __HANDLER__EVT_H__
#define __HANDLER__EVT_H__

#define evt_set(pool, events) ((pool) |= (events))
#define evt_clr(pool, events) ((pool) &= ~(events))
#define evt_has(pool, events) ((pool) & (events))
//#define evt_get(pool, events) (__sync_fetch_and_and(&(pool), ~(events)) & (events))

/*
static inline uint32_t evt_atomic_fetch_and_and(uint32_t *ptr, uint32_t val) {
  uint32_t old, new;
  do {
    old = mcu_exclusive_load_word(ptr);
    new = old & val;
  } while(mcu_exclusive_store_word(new, ptr));
  return old;
}


#define evt_get(pool, events) (evt_atomic_fetch_and_and(&(pool), ~(events)) & (events))
*/

#define evt_get(pool, events) ({   \
      mcu_data_mem_barrier();      \
      mcu_disable_interrupts();    \
      typeof(pool) __selected__ =  \
        (pool) & (events);         \
      (pool) &= ~(events);         \
      mcu_enable_interrupts();     \
      __selected__;                \
    })

#define evt_on(pool, events)             \
  for (; evt_has(pool, events) &&        \
         (evt_clr(pool, events) || 1); )

#endif /* __HANDLER__EVT_H__ */
