#ifndef __HANDLER__TMR_H__
#define __HANDLER__TMR_H__

#define tmr_set(timer, timeout) \
  (timer = (timeout))

#define tmr_on(timer, delta)    \
  for (timer = timer > delta ?  \
         timer - delta : timer; \
       timer > 0 &&             \
         timer <= delta;        \
       timer = 0)

#endif /* __HANDLER__TMR_H__ */
